
# ---------------------------------------------------------------------------------------------------------------------
# AWS RDS MySQL Database
# ---------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# REQUIRE A SPECIFIC TERRAFORM VERSION OR HIGHER
# This module uses 0.12 syntax, which means it is not compatible with any versions below 0.12.
# ----------------------------------------------------------------------------------------------------------------------
terraform {
  required_version = ">= 0.12"
}

# ---------------------------------------------------------------------------------------------------------------------
# Providers
# ---------------------------------------------------------------------------------------------------------------------
provider "random" {
  version = "~> 2.2"
}

# ---------------------------------------------------------------------------------------------------------------------
# Locals
# ---------------------------------------------------------------------------------------------------------------------

locals {
  name           = lower(var.db_name != null ? var.db_name : var.db_identifier)
  identifier     = lower("${var.environment}-${var.db_identifier}")
  engine         = "mysql"
  parameter_path = "/database/${local.engine}/${var.db_identifier}"

  # Common tags
  common_tags = merge(map(
    "Environment", var.environment,
    "ManagedBy", "Terraform"
  ), var.tags)

  replica_identifier     = "${local.identifier}-replica"
  replica_storage        = floor(var.db_storage * 1.2)
  replica_parameter_path = "/database/${local.engine}/${local.replica_identifier}"
}

# Find the VPC
data "aws_vpc" "vpc" {
  id = var.vpc_id
}

data "aws_subnet" "selected" {
  count = length(var.private_subnet_ids)
  id    = var.private_subnet_ids[count.index]
}

# Create the database subnet group
resource "aws_db_subnet_group" "main" {
  name       = local.identifier
  subnet_ids = var.private_subnet_ids
  tags = merge({
    Name = local.identifier
    },
    local.common_tags
  )
}

# Create the database security group and rules
resource "aws_security_group" "db" {
  name        = "${local.identifier}-sg"
  description = "${local.identifier} Security Group"
  vpc_id      = data.aws_vpc.vpc.id

  tags = merge({
    Name = local.identifier
    },
    local.common_tags
  )
}

resource "aws_security_group_rule" "db_ingress" {
  type        = "ingress"
  from_port   = var.port
  to_port     = var.port
  protocol    = "tcp"
  cidr_blocks = [for s in data.aws_subnet.selected : s.cidr_block]


  security_group_id = aws_security_group.db.id
}

resource "aws_security_group_rule" "db_egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = [for s in data.aws_subnet.selected : s.cidr_block]

  security_group_id = aws_security_group.db.id
}

# Generate a random sctring for the final snapshot
resource "random_string" "snapshot" {
  keepers = {
    # Generate a new id each time we change the db_identifier
    db_identifier = var.db_identifier
  }

  length           = 8
  override_special = "-"
}

# Create a KMS key to use for encryption at rest
resource "aws_kms_key" "db" {
  description = "${local.identifier}-rds-key"

  tags = merge({
    Name = local.identifier
    },
    local.common_tags
  )
}

# Generate a random string for the RDS password
resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!$#%"
}

# Create an AWS Secrets Mannager secret to store RDS auth credentials
resource "aws_secretsmanager_secret" "auth" {
  name = "database/${local.engine}/${var.db_identifier}/auth"

  recovery_window_in_days = 0
}

# Store the RDS credentials on the previously created secret.
resource "aws_secretsmanager_secret_version" "auth" {
  secret_id = aws_secretsmanager_secret.auth.id
  secret_string = jsonencode({
    username = var.username
    password = random_password.password.result
  })
}

# Write the DB address/endpoint/port to the parameter store
resource "aws_ssm_parameter" "db_address" {
  name        = "${local.parameter_path}/address"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.db.address
}

resource "aws_ssm_parameter" "db_port" {
  name        = "${local.parameter_path}/port"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.db.port
}

resource "aws_ssm_parameter" "db_endpoint" {
  name        = "${local.parameter_path}/endpoint"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.db.endpoint
}

# Create the database instance
resource "aws_db_instance" "db" {
  identifier        = local.identifier
  name              = local.name
  engine            = local.engine
  engine_version    = var.db_version
  allocated_storage = var.db_storage
  instance_class    = var.db_instance_class
  username          = var.username
  password          = random_password.password.result

  db_subnet_group_name            = aws_db_subnet_group.main.name
  multi_az                        = var.multi_az
  apply_immediately               = var.apply_immediately
  port                            = var.port
  iops                            = var.iops
  backup_retention_period         = var.backup_retention
  vpc_security_group_ids          = [aws_security_group.db.id]
  final_snapshot_identifier       = "${local.identifier}-final-${random_string.snapshot.result}x"
  storage_encrypted               = true
  kms_key_id                      = aws_kms_key.db.arn
  allow_major_version_upgrade     = var.allow_major_version_upgrade
  auto_minor_version_upgrade      = false
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  max_allocated_storage           = floor(var.db_storage * 1.1)
  delete_automated_backups        = false
  deletion_protection             = var.deletion_protection

  tags = merge({
    Name = local.identifier
    },
    local.common_tags
  )

  depends_on = [
    aws_cloudwatch_log_group.db_errors,
    aws_cloudwatch_log_group.db_audit,
    aws_cloudwatch_log_group.db_general,
    aws_cloudwatch_log_group.db_slowquery
  ]

  lifecycle {
    ignore_changes = [engine_version]
  }
}

resource "aws_sns_topic" "default" {
  name = "${local.identifier}-rds-events"
}

resource "aws_db_event_subscription" "default" {
  name      = "${local.identifier}-rds-event-sub"
  sns_topic = aws_sns_topic.default.arn

  source_type = "db-instance"
  source_ids  = [aws_db_instance.db.id]

  event_categories = [
    "failure",
    "low storage",
  ]
}

# Write the DB address/endpoint/port to the parameter store
resource "aws_ssm_parameter" "replica_db_address" {
  count = var.read_replica ? 1 : 0

  name        = "${local.replica_parameter_path}/address"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.replica[0].address
}

resource "aws_ssm_parameter" "replica_db_port" {
  count = var.read_replica ? 1 : 0

  name        = "${local.replica_parameter_path}/port"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.replica[0].port
}

resource "aws_ssm_parameter" "replica_db_endpoint" {
  count = var.read_replica ? 1 : 0

  name        = "${local.replica_parameter_path}/endpoint"
  type        = "String"
  description = "Created by Terraform - do not edit"
  value       = aws_db_instance.replica[0].endpoint
}

# Create the database instance
resource "aws_db_instance" "replica" {
  count = var.read_replica ? 1 : 0

  identifier          = local.replica_identifier
  engine              = local.engine
  engine_version      = var.db_version
  allocated_storage   = local.replica_storage
  instance_class      = var.read_replica_instance_class
  username            = var.username
  password            = random_password.password.result
  replicate_source_db = aws_db_instance.db.identifier

  multi_az                        = var.multi_az
  apply_immediately               = var.apply_immediately
  port                            = var.port
  iops                            = var.iops
  vpc_security_group_ids          = [aws_security_group.db.id]
  storage_encrypted               = true
  kms_key_id                      = aws_kms_key.db.arn
  allow_major_version_upgrade     = var.allow_major_version_upgrade
  auto_minor_version_upgrade      = false
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  max_allocated_storage           = floor(local.replica_storage * 1.1)
  skip_final_snapshot             = true
  deletion_protection             = var.deletion_protection

  tags = merge({
    Name = local.replica_identifier
    },
    local.common_tags
  )

  depends_on = [
    aws_cloudwatch_log_group.db_errors_replica,
    aws_cloudwatch_log_group.db_audit_replica,
    aws_cloudwatch_log_group.db_general_replica,
    aws_cloudwatch_log_group.db_slowquery_replica
  ]

  lifecycle {
    ignore_changes = [engine_version]
  }
}

resource "aws_sns_topic" "replica" {
  count = var.read_replica ? 1 : 0

  name = "${local.replica_identifier}-rds-events"
}

resource "aws_db_event_subscription" "replica" {
  count = var.read_replica ? 1 : 0

  name      = "${local.replica_identifier}-rds-event-sub"
  sns_topic = aws_sns_topic.replica[0].arn

  source_type = "db-instance"
  source_ids  = [aws_db_instance.replica[0].id]

  event_categories = [
    "failure",
    "low storage",
  ]
}

# Create CloudWatch log groups with 14 day expiry
resource "aws_cloudwatch_log_group" "db_errors" {
  name              = "/aws/rds/instance/${local.identifier}/error"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_audit" {
  name              = "/aws/rds/instance/${local.identifier}/audit"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_general" {
  name              = "/aws/rds/instance/${local.identifier}/general"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_slowquery" {
  name              = "/aws/rds/instance/${local.identifier}/slowquery"
  retention_in_days = 14
}
# Create CloudWatch log groups of replica with 14 day expiry
resource "aws_cloudwatch_log_group" "db_errors_replica" {
  count             = var.read_replica ? 1 : 0
  name              = "/aws/rds/instance/${local.replica_identifier}/error"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_audit_replica" {
  count             = var.read_replica ? 1 : 0
  name              = "/aws/rds/instance/${local.replica_identifier}/audit"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_general_replica" {
  count             = var.read_replica ? 1 : 0
  name              = "/aws/rds/instance/${local.replica_identifier}/general"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "db_slowquery_replica" {
  count             = var.read_replica ? 1 : 0
  name              = "/aws/rds/instance/${local.replica_identifier}/slowquery"
  retention_in_days = 14
}
